<?php
//Template Name: Index
?>

<?php get_header(); ?>
    <main>
        <section class="slide">
            <div class="slideContainer">
                <h1><?php the_field('lobo_titulo'); ?></h1>
                <div class="barSlide"></div>
                <p><?php the_field('lobo_descricao'); ?></p>
            </div>
        </section>

        <section class="about">
            <h3><?php the_field('titulo_sobre'); ?></h3>
            <p><?php the_field('descricao_sobre'); ?></p>
        </section>

        <section class="values">
            <h3><?php the_field('valores_titulo'); ?></h3>
            <div class="valueContainer">
                <div>
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/life-insurance.svg" alt="">
                    </figure>
                    <h4><?php the_field('valor1'); ?></h4>
                    <p><?php the_field('descricao1'); ?></p>
                </div>
                <div>
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/care.svg" alt="">
                    </figure>
                    <h4><?php the_field('valor2'); ?></h4>
                    <p><?php the_field('descricao2'); ?></p>
                </div>
                <div>
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/Frame.svg" alt="">
                    </figure>
                    <h4><?php the_field('valor3'); ?></h4>
                    <p><?php the_field('descricao3'); ?></p>
                </div>
                <div>
                    <figure>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/rescue-dog.svg" alt="">
                    </figure>
                    <h4><?php the_field('valor4'); ?></h4>
                    <p><?php the_field('descricao4'); ?></p>
                </div>
            </div>
        </section>

        <section class="lobosExemplo">
            <?php $the_query = new WP_Query('posts_per_page=2'); ?>

            <h3><?php the_field('lobos_titulo'); ?></h3>

            <section class="lobinhos">
                <figure class="lobos">
                    <?php if( get_field('foto_do_lobo') ): ?>
                    <img src="<?php the_field('lobo-foto'); ?>" alt="">
                    <?php endif; ?>
                </figure>
                <h2 class="bloco1"><?php the_field('lobo-titulo'); ?></h2>
                <h3 class="bloco1">Idade: <?php the_field('lobo-idade'); ?> anos</h3>
                <h3 class="bloco1"><?php the_field('lobo-descricao'); ?></h3>
            </section>
        </section>

    </main>
<?php get_footer(); ?>
   