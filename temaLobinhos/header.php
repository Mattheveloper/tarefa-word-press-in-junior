<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Alata&family=Darker+Grotesque&family=Libre+Bodoni:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <title><?php bloginfo('Adote um Lobinho') ?></title>
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <a href="/listaDeLobinhos.php" class="headerText">Nossos Lobinhos</a>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/iconLobinho.svg" alt="">
        <div class="quemSomos">
            <a href="quemSomos.php" class="headerText">Quem Somos</a>
            <div class="bar"></div>
        </div>
    </header>