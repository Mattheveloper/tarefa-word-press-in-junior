<?php
//Template Name: Lista de Lobinhos
?>

<?php get_header(); ?>
    <main>
            <figure class="lobos">
                <?php if( get_field('lobo-foto') ): ?>
                <img src="<?php the_field('lobo-foto'); ?>" alt="">
                <?php endif; ?>
            </figure>
            <div class="bloco1">
                <h2 class="titulo1"><?php the_field('lobo-titulo'); ?></h2>
                <h3 class="idade1">Idade: <?php the_field('lobo-idade'); ?> anos</h3>
                <h3 class="descricao1"><?php the_field('lobo-descricao'); ?></h3>
            </div>
            
            <figure class="lobos">
                <?php if( get_field('lobo-foto') ): ?>
                <img src="<?php the_field('lobo-foto'); ?>" alt="">
                <?php endif; ?>
            </figure>
            <div class="bloco2">
                <h2 class="titulo2"><?php the_field('lobo-titulo'); ?></h2>
                <h3 class="idade2">Idade: <?php the_field('lobo-idade'); ?> anos</h3>
                <h3 class="descricao2"><?php the_field('lobo-descricao'); ?></h3>
            </div>
    </main>

    <?php get_footer(); ?>